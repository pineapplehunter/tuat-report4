#!/bin/bash

if [ ! -d build ]; then
    CC=clang CXX=clang++ meson build
fi

ninja -C build
if [ "$?" == "0" ]; then
    ./build/src/score-info $@
fi
