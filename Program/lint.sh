#!/bin/bash

clang-format -i --style Google src/*.cc src/*.h
ninja scan-build -C build
