#include <iostream>

void write_to(const char *filename, const std::string &str) {
  FILE *fp = fopen(filename, "w");
  if (fp == nullptr) {
    std::cerr << "could not write to file '" << filename << "'" << std::endl;
    return;
  }

  for (const char &c : str) {
    fputc(c, fp);
  }
}
