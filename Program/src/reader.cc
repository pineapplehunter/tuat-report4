#include <iostream>

std::string read_file(const char *filename) {
  std::string tensu = "";

  FILE *fp = fopen(filename, "r");

  if (fp == nullptr) {
    std::cerr << "could not open " << filename << std::endl;
    return "";
  }

  int v;
  while ((v = fgetc(fp)) >= 0) {
    if ((char)v == '\n') v = ' ';
    tensu += (char)v;
  }

  fclose(fp);

  return tensu;
}

std::string read_stdin() {
  std::string tensu = "";

  int v;
  while ((v = std::getchar()) >= 0) {
    if ((char)v == '\n') v = ' ';
    tensu += (char)v;
  }

  return tensu;
}
