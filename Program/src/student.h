#ifndef STUDENT
#define STUDENT

#include <iostream>
#include <vector>

namespace student {

typedef struct {
  char FirstName[32];
  char FamilyName[32];
  int score[5];
} Student;

std::vector<Student> parse(const std::string &);
void format(std::vector<Student> &);
void sort(std::vector<Student> &);

std::string get_name(const Student &);
float get_average(const Student &);
float get_average_all(const std::vector<Student> &);
float get_standerd_deviation(const std::vector<Student> &);
float get_standerd_score(const Student &, float, float);

}  // namespace student

#endif
