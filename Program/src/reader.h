#ifndef READER
#define READER

#include <iostream>

std::string read_file(const char *);
std::string read_stdin();

#endif
