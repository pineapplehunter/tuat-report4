#include <iostream>
#include <vector>

#include "reader.h"
#include "student.h"
#include "writer.h"

using student::Student;

int main(int argc, char const *argv[]) {
  // 点数ファイルの読み込み
  std::string tensu;
  if (argc == 1) {
    tensu = read_stdin();
  } else if (argc == 2) {
    tensu = read_file(argv[1]);
  } else {
    std::cerr << "too many arguments" << std::endl;
    return -1;
  }

  // 文字列から生徒情報の抽出
  std::vector<Student> students = student::parse(tensu);
  student::format(students);
  student::sort(students);

  // 全体の点数の平均
  float avg_all = student::get_average_all(students);
  // 標準偏差
  float sd = student::get_standerd_deviation(students);

  // 書き込む文字列の準備
  std::string output_str = "";
  output_str += "Average           : " + std::to_string(avg_all) + "\n";
  output_str += "Standerd deviation: " + std::to_string(sd) + "\n\n";
  output_str += "Standerd score\tName\n";

  for (const Student &s : students) {
    float ss = student::get_standerd_score(s, avg_all, sd);
    std::string ss_str = std::to_string(ss);
    if (14 > ss_str.size()) ss_str.insert(0, 14 - ss_str.size(), ' ');
    output_str += ss_str + "\t" + student::get_name(s) + "\n";
  }

  // ファイルに書き出す
  write_to("seiseki.txt", output_str);

  return 0;
}
