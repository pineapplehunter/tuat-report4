#include "student.h"
#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

namespace student {

std::vector<Student> parse(const std::string &tensu) {
  std::vector<Student> students;

  int index = 0;
  while (true) {
    // initalize new student
    Student s = {.FirstName = {0}, .FamilyName = {0}, .score = {0}};

    // skip spaces
    while (tensu[index] == ' ') index++;
    // stop if null
    if (tensu[index] == '\0') break;

    // read family name
    for (int offset = index; tensu[index] != ' '; index++) {
      s.FamilyName[index - offset] = tensu[index];
    }

    // skip spaces
    while (tensu[index] == ' ') index++;

    // read first name
    for (int offset = index; tensu[index] != ' '; index++) {
      s.FirstName[index - offset] = tensu[index];
    }

    // read scores
    for (int i = 0; i < 5; i++) {
      while (tensu[index] == ' ') index++;
      std::string tmp = "";
      // check if value is in 0 to 9
      for (; '0' <= tensu[index] && tensu[index] <= '9'; index++) {
        tmp += tensu[index];
      }
      s.score[i] = std::stoi(tmp);
    }

    // add student to list
    students.push_back(s);
  }
  return students;
}

void format(std::vector<Student> &students) {
  for (Student &s : students) {
    for (int i = 0; i < 32; i++) {
      s.FamilyName[i] = std::toupper(s.FamilyName[i]);
    }

    s.FirstName[0] = std::toupper(s.FirstName[0]);
    for (int i = 1; i < 32; i++) {
      s.FirstName[i] = std::tolower(s.FirstName[i]);
    }
  }
}

std::string get_name(const Student &s) {
  std::string name = "";
  for (int i = 0; i < 32 && s.FirstName[i]; i++) {
    name += s.FirstName[i];
  }
  name += ' ';
  for (int i = 0; i < 32 && s.FamilyName[i]; i++) {
    name += s.FamilyName[i];
  }
  return name;
}

bool compare(const Student &a, const Student &b) {
  for (int i = 0; i < 32; i++) {
    if (a.FirstName[i] == b.FirstName[i]) continue;
    return a.FirstName[i] < b.FirstName[i];
  }
  for (int i = 0; i < 32; i++) {
    if (a.FamilyName[i] == b.FamilyName[i]) continue;
    return a.FamilyName[i] < b.FamilyName[i];
  }
  return false;
}

void sort(std::vector<Student> &students) {
  std::sort(students.begin(), students.end(), compare);
}

float get_average(const Student &s) {
  float avg = .0;
  for (int i = 0; i < 5; i++) avg += s.score[i];
  avg /= 5.0;
  return avg;
}

float get_average_all(const std::vector<Student> &students) {
  float avg_all = .0;
  for (const Student &s : students) avg_all += get_average(s);
  avg_all /= (float)students.size();
  return avg_all;
}

float get_standerd_deviation(const std::vector<Student> &students) {
  float avg_all = get_average_all(students);

  float sd = .0;
  for (const Student &s : students)
    sd += std::pow(get_average(s) - avg_all, 2.);
  sd /= students.size();
  sd = std::sqrt(sd);

  return sd;
}

float get_standerd_score(const Student &s, float avg_all, float sd) {
  float ss;
  ss = get_average(s) - avg_all;
  ss *= 10;
  ss /= sd;
  ss += 50;
  return ss;
}

}  // namespace student