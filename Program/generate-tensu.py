import random, string

letters = string.ascii_lowercase + string.ascii_uppercase

f = open("tensu.txt","w")

for _ in range(50):
    familyname = ''.join([random.choice(letters) for _ in range(random.randrange(1,32))])
    firstname = ''.join([random.choice(letters) for _ in range(random.randrange(1,32))])
    score = [random.randrange(0,100) for _ in range(5)]

    s = ""
    s += familyname + " "
    s += firstname + " "
    for n in score:
        s += str(n) + " "
    s += "\n"
    f.write(s)

f.close()